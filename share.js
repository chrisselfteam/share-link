chrome.tabs.query({
    active: true,
    lastFocusedWindow: true
}, function(tabs) {
    // and use that tab to fill in out title and url
    var tab = tabs[0];
    var target = "http://localhost:8000/share.html?target=" + tab.url;
    document.getElementById("share-iframe").setAttribute("src", target);
});